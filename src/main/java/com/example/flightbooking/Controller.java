package com.example.flightbooking;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;


@RestController
public class Controller {

    @Autowired
    FlightService flightService;

    @Autowired
    TicketService ticketService;



    @PostMapping("/api/v1.0/flight/airline/inventory/add")
    public String addFlight(@RequestBody Flight flight) {

        flightService.save(flight);
        return "Registered Flight :" + flight.getFlightName();
    }

    @PostMapping("/api/v1.0/flight/booking/{flightid}")
    public double bookTicket(@PathVariable String flightid){
        Ticket ticket = new Ticket();
        ticket.setFlightId(flightid);
        double pnr = Math.random();
        ticket.setTicketNo(pnr);

        return pnr;

    }



    @PostMapping("/api/v1.0/flight/admin/login")
    public void adminLogin(@RequestHeader String username, @RequestHeader String password) {
        Admin admin = new Admin(username,password);

    }

    @GetMapping("/api/v1.0/flight/ticket/{pnr}")
    public Ticket getTicket(@PathVariable double pnr){
        Ticket ticket = new Ticket();
        ticket= ticketService.findTicket(pnr);

        return ticket;
    }

    @PostMapping("/api/v1.0/flight/search")
    public String searchFlight(String flightname){

        List<Flight> registeredFlights = new ArrayList<>();
        registeredFlights= flightService.getAllFlights();

        for(Flight flight : registeredFlights){
            if(flightname.equals(flight.getFlightName())){
                return flight.toString();
            }
        }
        return  "Flight not found ";
    }
}