package com.example.flightbooking;

import java.util.List;

public class Ticket {

    private double ticketNo;
    private String name;
    private String email;
    private int noOfSeats;
    private String flightId;

    public void setTicketNo(double ticketNo) {
        this.ticketNo = ticketNo;
    }

    public double getTicketNo() {
        return ticketNo;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    private List<Passenger> passengerList;
    private String flightName;
    private long amount;

}
