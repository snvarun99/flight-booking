package com.example.flightbooking;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class FlightService {

    @Autowired
    FlightRepo flightRepo;


    public void save(Flight flight){

        flightRepo.save(flight);
    }

    public List<Flight> getAllFlights(){

        List<Flight> flightList = new ArrayList<>();

         flightList=flightRepo.findAll();

         return  flightList;
    }
}
