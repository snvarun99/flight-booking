package com.example.flightbooking;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TicketRepo  extends JpaRepository {
}
