package com.example.flightbooking;

import org.springframework.beans.factory.annotation.Autowired;

public class TicketService {

    @Autowired
    TicketRepo ticketRepo;

    public Ticket findTicket(double pnr){

       Ticket ticket = (Ticket) ticketRepo.getById(pnr);
       return ticket;
    }
}
