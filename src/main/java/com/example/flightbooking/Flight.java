package com.example.flightbooking;

import org.joda.time.DateTime;

public class Flight {

    private int flightNo;
    private int flightName;

    @Override
    public String toString() {
        return "Flight{" +
                "flightNo=" + flightNo +
                ", flightName=" + flightName +
                ", onBoardingLocation='" + onBoardingLocation + '\'' +
                ", destination='" + destination + '\'' +
                ", startDateTime=" + startDateTime +
                ", endDateTime=" + endDateTime +
                '}';
    }

    public int getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(int flightNo) {
        this.flightNo = flightNo;
    }

    public int getFlightName() {
        return flightName;
    }

    public void setFlightName(int flightName) {
        this.flightName = flightName;
    }

    public String getOnBoardingLocation() {
        return onBoardingLocation;
    }

    public void setOnBoardingLocation(String onBoardingLocation) {
        this.onBoardingLocation = onBoardingLocation;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public DateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(DateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public DateTime getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(DateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    private String onBoardingLocation;
    private String destination;
    private DateTime startDateTime;
    private DateTime endDateTime;

}
